api-spec-converter --from=openapi_3 --to=swagger_2 ../rental-handler/rental-handler-api/src/main/resources/openapi/rental-handler-api.yaml > swaggers/rental-handler.json
api-spec-converter --from=openapi_3 --to=swagger_2 ../booking-handler/booking-handler-api/src/main/resources/openapi/booking-handler-api.yaml > swaggers/booking-handler.json
flutter pub run build_runner build
