import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:front/common/const.dart';
import 'package:front/pages/bookings/confirmation.dart';
import 'package:front/pages/create_rental/create_rental.dart';
import 'package:front/pages/favorites.dart';
import 'package:front/pages/auth_middleware.dart';
import 'package:front/pages/groups/page.dart';
import 'package:front/pages/rentals/rental_results.dart';
import 'package:front/pages/rentals/my_rentals/my_rental.dart';
import 'package:front/pages/rentals/my_rentals/my_rentals.dart';
import 'package:front/pages/profile/personal_info.dart';
import 'package:front/pages/profile/profile.dart';
import 'package:front/pages/rentals/rental.dart';
import 'package:front/pages/bookings/bookings.dart';

FluroRouter getRouter() {
  final router = FluroRouter();
  router.notFoundHandler = Handler(handlerFunc: (context, params) {
    return const Scaffold(body: Center(child: Text("Page not found")));
  });

  // ------ WITH ARGS ------
  router.define(rentalUrl, transitionDuration: Duration.zero,
      handler: Handler(handlerFunc: (context, params) {
    return AuthMiddleware(RentalPage(rentalId: params['id']![0]));
  }));
  router.define(myRentalUrl, transitionDuration: Duration.zero,
      handler: Handler(handlerFunc: (context, params) {
    return AuthMiddleware(MyRentalPage(rentalId: params['id']![0]));
  }));
  router.define(personalInfoUrl, transitionDuration: Duration.zero,
      handler: Handler(handlerFunc: (context, params) {
    return const AuthMiddleware(PersonalInfoPage());
  }));

  router.define(createBookingUrl, transitionDuration: Duration.zero,
      handler: Handler(handlerFunc: (context, params) {
    return AuthMiddleware(BookingConfirmationPage(rentalId: params['id']![0]));
  }));

  // ------ NO ARGS ------
  _addNoArgsRoute(router, homeUrl, const HomePage());
  _addNoArgsRoute(router, profileUrl, const ProfilePage());
  _addNoArgsRoute(router, signUrl, const ProfilePage());
  _addNoArgsRoute(router, favoritesUrl, const FavoritesPage());
  _addNoArgsRoute(router, groupsUrl, const GroupsPage());
  _addNoArgsRoute(router, reservationsUrl, const ReservationsPage());
  _addNoArgsRoute(router, myRentalsUrl, const MyRentalsPage());
  _addNoArgsRoute(router, createRentalUrl, const CreateRentalPage());
  return router;
}

_addNoArgsRoute(router, url, page) {
  router.define(url, transitionDuration: Duration.zero,
      handler: Handler(handlerFunc: (context, params) {
    return AuthMiddleware(page);
  }));
}
