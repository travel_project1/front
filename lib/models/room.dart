import 'dart:convert';

import 'package:front/generated_models/rental_handler.swagger.dart';
import 'package:front/models/equipment.dart';

class Room {
  final List<Equipment> equipments;
  final String name;

  Room({required this.equipments, required this.name});

  static Room toRoom(RoomDto roomDto) {
    return Room(
        equipments: roomDto.equipments!
            .map((equipment) => Equipment.toEquipment(equipment))
            .toList(),
        name: roomDto.name ?? "unamed");
  }

  Map<String, dynamic> toJson() => {'name': name, 'equipments': equipments};

  @override
  String toString() {
    return "[name] " + equipments.join(",");
  }
}
