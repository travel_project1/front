class Feature {
  final int id;
  final String name;
  final String description;

  Feature({required this.id, required this.name, required this.description});

  factory Feature.fromJson(Map<String, dynamic> json) {
    return Feature(id: 2, name: json['name'], description: json['description']);
  }

  Map<String, dynamic> toJson() => {'name': name, 'description': description};
}
