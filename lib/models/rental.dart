import 'dart:math';

import 'package:front/common/utils.dart';
import 'package:front/generated_models/rental_handler.swagger.dart';
import 'package:front/models/availability.dart';
import 'package:front/models/rental_criteria.dart';

class Rental {
  int id;
  String name;
  String description;
  String imagePath;
  int size;
  String city;
  String address;
  String zipCode;
  List<RentalCriteria> criterias;
  List<Availability> availabilities;

  Rental(
      {required this.id,
      required this.name,
      required this.description,
      required this.imagePath,
      required this.size,
      required this.city,
      required this.address,
      required this.zipCode,
      required this.criterias,
      required this.availabilities});

  static Rental toRental(RentalDto rentalDto) {
    final randomImagePreview = Random().nextInt(5);
    return Rental(
        id: rentalDto.id ?? 0,
        name: rentalDto.name ?? "unnamed",
        description: rentalDto.description ?? "",
        imagePath: "assets/images/preview_$randomImagePreview.jpg",
        size: rentalDto.size ?? -1,
        city: rentalDto.city ?? "no city",
        address: rentalDto.address ?? "no address",
        zipCode: rentalDto.zipCode ?? "00000",
        criterias: [],
        availabilities: []);
  }

  Map<String, dynamic> toJson() => {
        "name": name,
        'description': description,
        //'imagePath': imagePath,
        'size': size,
        'city': city,
        'address': address,
        'zipCode': zipCode,
        'availabilities': [Availability(DateTime(2022, 04, 01), DateTime(2022, 0,4, 18))],
        'criterias': [],
        'rooms': []
      };

  static Rental empty() {
    return Rental(
        id: -1,
        name: "",
        description: "",
        imagePath: "",
        size: -1,
        city: "",
        address: "",
        zipCode: "",
        criterias: [],
        availabilities: []);
  }

  void pushAvailabilities(DateTime date) {
    if (availabilities.isEmpty) {
      availabilities.add(Availability(date, null));
    } else {
      availabilities[0].end = date;
    }
  }

  void setCriteriaIntValue(int id, int value) {
    for (var crit in criterias) {
      if (crit.id == id) {
        crit.intValue = value;
      }
    }
  }

  void setCriteriaBoolValue(int id, bool value) {
    for (var crit in criterias) {
      if (crit.id == id) {
        crit.boolValue = value;
      }
    }
  }
}
