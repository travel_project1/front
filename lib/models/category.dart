import 'package:front/models/criteria.dart';

class Category {
  final String name;
  final String id;
  final bool isMandatory;
  final bool isUnique;

  Category(this.name, this.id, this.isMandatory, this.isUnique);

  factory Category.fromJson(dynamic json) {
    return Category(
        json["name"], json["id"], json["isMandatory"], json["isUnique"]);
  }
}
