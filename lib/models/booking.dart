import 'package:front/common/utils.dart';
import 'package:front/generated_models/booking_handler.swagger.dart';
import 'package:intl/intl.dart';

class Booking {
  int id;
  int rentalId;
  DateTime begin;
  DateTime end;

  Booking({
    required this.id,
    required this.rentalId,
    required this.begin,
    required this.end,
  });

  static Booking toBooking(BookingDto bookingDto) {
    return Booking(
        id: bookingDto.id ?? 0,
        rentalId: bookingDto.rentalId ?? 0,
        begin: bookingDto.begin ?? DateTime.now(),
        end: bookingDto.end ?? DateTime.now());
  }

  Map<String, dynamic> toJson() => {
        "rentalId": rentalId,
        'begin': formatDate(begin),
        'end': formatDate(end)
      };

  static Booking empty() {
    return Booking(
      id: -1,
      rentalId: -1,
      begin: DateTime.now(),
      end: DateTime.now(),
    );
  }
}
