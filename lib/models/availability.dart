import 'package:front/common/utils.dart';

class Availability {
  final DateTime begin;
  DateTime? end;

  Availability(this.begin, this.end);

  Map<String, dynamic> toJson() => {
    "beginDate": formatDate(begin),
    "endDate": formatDate(end!)
  };
}