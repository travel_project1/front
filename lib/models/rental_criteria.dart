import 'package:front/models/criteria.dart';

class RentalCriteria {
  int id;
  String name;
  bool hasValue;
  int? intValue;
  bool? boolValue;

  RentalCriteria(this.id, this.name, this.hasValue) {
    if (hasValue) {
      intValue = 1;
    } else {
      boolValue = true;
    }
  }

  factory RentalCriteria.fromCriteria(Criteria crit) {
    return RentalCriteria(crit.id, crit.name, crit.hasValue);
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "value": hasValue ? intValue! : boolValue!
      };
}
