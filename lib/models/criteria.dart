class Criteria {
  final int id;
  final String name;
  final bool hasValue;

  Criteria(this.id, this.name, this.hasValue);

  factory Criteria.fromJson(dynamic json) {
    return Criteria(json["id"], json["name"], json["hasValue"]);
  }
}
