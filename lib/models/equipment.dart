import 'package:front/generated_models/rental_handler.swagger.dart';

class Equipment {
  final String name;

  Equipment({required this.name});

  static Equipment toEquipment(EquipmentDto equipmentDto) {
    return Equipment(name: equipmentDto.name ?? "unamed");
  }

  Map<String, dynamic> toJson() => {'name': name};

  @override
  String toString() {
    return name;
  }
}
