import 'dart:convert';
import 'dart:io';
import 'package:front/common/const.dart';
import 'package:front/models/booking.dart';
import 'package:front/services/auth.dart';
import 'package:http/http.dart' as http;

enum BackendService { rental, criteria, booking }

extension BackendServiceURL on BackendService {
  String get url {
    switch (this) {
      case BackendService.rental:
        return backendRentalUrl;
      case BackendService.criteria:
        return backendCriteriaUrl;
      case BackendService.booking:
        return backendBookingUrl;
    }
  }
}

class ApiService {
  Future<http.Response> get(BackendService service, String uri) async {
    String currentIdToken =
        await Authenticator().authenticatedUser!.getIdToken();
    return await http.get(
      Uri.parse(service.url + uri),
      headers: {HttpHeaders.authorizationHeader: 'Bearer ' + currentIdToken},
    );
  }

  Future<http.Response> post(
      BackendService service, String uri, Object body) async {
    String currentIdToken =
        await Authenticator().authenticatedUser!.getIdToken();

    return await http.post(Uri.parse(service.url + uri),
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer ' + currentIdToken,
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode(body));
  }
}
