import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:google_sign_in/google_sign_in.dart';

// Service which is making the authentication easier.
// It's a singleton so you can access it anywhere and it makes sure that Firebase is initialized once.
// It needs to be initialized before using it: [firebaseInitialized] has to be set to true.
class Authenticator {
  bool firebaseInitialized = false;

  User? get authenticatedUser => FirebaseAuth.instance.currentUser;

  bool get isAuthenticated => FirebaseAuth.instance.currentUser != null;

  Future<void> logout() async {
    await FirebaseAuth.instance.signOut();
  }

  Future<bool> initialize() async {
    try {
      await Firebase.initializeApp();
      firebaseInitialized = true;
    } catch (error) {
      firebaseInitialized = false;
    }

    return firebaseInitialized;
  }

  Future<UserCredential> emailSignUp(String email, String password) async {
    final auth = FirebaseAuth.instance;
    return auth.createUserWithEmailAndPassword(
        email: email, password: password);
  }

  Future<UserCredential> emailAuth(String email, String password) async {
    final auth = FirebaseAuth.instance;
    return auth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<UserCredential> googleAuth() async {
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return FirebaseAuth.instance.signInWithCredential(credential);
  }

  // Singleton logic
  static final Authenticator _instance = Authenticator._internal();

  factory Authenticator() {
    return _instance;
  }

  Authenticator._internal();
}
