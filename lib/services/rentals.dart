import 'dart:convert';
import 'package:front/generated_models/rental_handler.swagger.dart';
import 'package:front/models/rental.dart';
import 'package:front/services/api.dart';
import 'package:http/http.dart' as http;

class RentalsService {
  Future<List<Rental>> getRentals() async {
    final http.Response response;
    try {
      response = await ApiService().get(BackendService.rental, '/rentals');
    } catch (e) {
      rethrow;
    }

    final responseJson = jsonDecode(response.body) as List;
    return responseJson
        .map((rental) => Rental.toRental(RentalDto.fromJson(rental)))
        .toList();
  }

  Future<Rental> getRental(rentalId) async {
    // TODO(LG): Remove this ! Only to test UI loading system !!
    return Rental(
        id: 1,
        name: "Appartement Hawai",
        description: "Description de l'appartement d'Hawai",
        imagePath: 'assets/images/landscape.jpeg',
        size: 50,
        city: "Paris",
        address: "6 allee",
        zipCode: "14110",
        criterias: [],
        availabilities: []);
  }

  Future<http.Response> createRental(Rental rental) async {
    final http.Response response;
    try {
      response =
          await ApiService().post(BackendService.rental, '/rentals', rental);
    } catch (e) {
      rethrow;
    }

    return response;
  }
}
