import 'package:http/http.dart' as http;

import 'package:front/models/rental.dart';
import 'package:front/services/api.dart';

class FavoritesService {
  Future<List<Rental>> getFavorites() async {
    final http.Response response;
    try {
      response = await ApiService().get(BackendService.rental, '/rentals');
    } catch (e) {
      rethrow;
    }

    // final responseJson = jsonDecode(response.body) as List;
    // return responseJson
    //     .map((rental) => Rental.toRental(RentalDto.fromJson(rental)))
    //     .toList();
    return [
      Rental(
          id: 22,
          name: "Voyage Hawai",
          description:
              "Bel appartement de 50m² situé en plein centre de la charmante cité. A deux pas des commerces et d’un centre ...",
          imagePath: "assets/images/landscape.jpeg",
          size: 42,
          city: "Paris",
          address: "6 allee",
          zipCode: "14110",
          criterias: [],
          availabilities: [])
    ];
  }
}
