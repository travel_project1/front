import 'dart:convert';

import 'package:front/models/category.dart';
import 'package:front/models/criteria.dart';
import 'package:front/services/api.dart';
import 'package:http/http.dart' as http;

class CriteriasService {
  Future<List<Category>> getCategories() async {
    final http.Response response;
    try {
      response = await ApiService().get(BackendService.criteria, '/categories');
    } catch (e) {
      rethrow;
    }

    final responseJson = jsonDecode(response.body) as List;
    return responseJson.map((category) => Category.fromJson(category)).toList();
  }

  Future<List<Criteria>> getCriterias(String category) async {
    final http.Response response;

    try {
      response = await ApiService().get(BackendService.criteria, '/criterias');
    } catch (e) {
      rethrow;
    }

    final responseJson = jsonDecode(response.body) as List;
    return responseJson.map((criteria) => Criteria.fromJson(criteria)).toList();
  }

  // Singleton logic
  static final CriteriasService _instance = CriteriasService._internal();

  factory CriteriasService() {
    return _instance;
  }

  CriteriasService._internal();
}
