import 'dart:convert';
import 'package:front/generated_models/booking_handler.swagger.dart';
import 'package:front/models/booking.dart';
import 'package:front/services/api.dart';
import 'package:http/http.dart' as http;

class BookingsService {
  Future<List<Booking>> getBookings() async {
    final http.Response response;
    response = await ApiService().get(BackendService.booking, '/bookings');

    final responseJson = jsonDecode(response.body) as List;
    return responseJson
        .map((booking) => Booking.toBooking(BookingDto.fromJson(booking)))
        .toList();
  }

  Future<Booking> getBooking(bookingId) async {
    // TODO(LG): Remove this ! Only to test UI loading system !!
    return Booking(
        id: 1, rentalId: 1, begin: DateTime.now(), end: DateTime.now());
  }

  Future<http.Response> createBooking(Booking booking) async {
    final http.Response response;
    response =
        await ApiService().post(BackendService.booking, '/bookings', booking);

    return response;
  }
}
