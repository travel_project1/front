import 'package:front/models/group.dart';

class GroupsService {
  Future<List<Group>> getGroups() async {
    return [
      Group(name: "Toussaint"),
      Group(name: "LES COPAINS"),
      Group(name: "Ski"),
      Group(name: "Anniversaire Toto famille"),
      Group(name: "Anniversaire Toto potes"),
      Group(name: "ALED"),
      Group(name: "JPP de fou"),
      Group(name: "Pourquoi moi ???")
    ];
  }
}
