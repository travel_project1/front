import 'package:intl/intl.dart';

String formatDate(DateTime date) {
  return "${DateFormat('yyyy-MM-dd').format(date)}T00:00:00+00:00";
}