const double topPageMargin = 50;

const int favoritesIndex = 2;
const int groupsIndex = 1;
const int homeIndex = 0;
const int reservationsIndex = 3;
const int profileIndex = 4;

const homeUrl = "/rentals";
const rentalUrl = "/rentals/:id";
const createBookingUrl = "/rentals/:id/booking";
const createRentalUrl = "/profile/create/rentals";

const myRentalsUrl = "/profile/rentals";
const myRentalUrl = "/profile/rentals/:id";
const profileUrl = "/profile";

const favoritesUrl = "/favorites";
const groupsUrl = "/groups";
const reservationsUrl = "/reservations";
const signUrl = "/sign";
const personalInfoUrl = "/profile/me";

const String baseBackendUrl = 'http://192.168.43.17';
const String backendRentalUrl = '$baseBackendUrl:8002/api/v1';
const String backendBookingUrl = '$baseBackendUrl:8001/api/v1';
const String backendCriteriaUrl = '$baseBackendUrl:8003/api/v1';
