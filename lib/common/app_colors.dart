import 'package:flutter/material.dart';

const primaryColor = Color(0xff67f5e0);
const blueColor = Color(0xff6895de);
const purpleColor = Color(0xff9d67f5);
const pinkColor = Color(0xffeb63c0);
const greenColor = Color(0xff6cf070);
const greyColor = Color(0xff696969);
