import 'package:flutter/material.dart';
import 'package:front/pages/signIn/page.dart';
import 'package:front/services/auth.dart';

class AuthMiddleware extends StatelessWidget {
  final Widget _page;

  const AuthMiddleware(this._page, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authenticator = Authenticator();

    if (authenticator.firebaseInitialized) {
      return _page;
    }

    final Future<bool> firebaseInitializer = authenticator.initialize();

    return Scaffold(
        body: FutureBuilder(
            future: firebaseInitializer,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _getError();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                if (Authenticator().authenticatedUser == null) {
                  return const SignInPage();
                }

                return _page;
              }

              return _getLoading();
            }));
  }

  _getError() {
    return const Center(child: Text("Error initializing Firebase"));
  }

  _getLoading() {
    return const Center(child: CircularProgressIndicator(color: Colors.blue));
  }
}
