import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/services/auth.dart';

class CredentialAuth extends StatefulWidget {
  const CredentialAuth({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CredentialAuth();
}

class SignUp {
  String email;
  String password;

  SignUp({this.email = "", this.password = ""});
}

class _CredentialAuth extends State<CredentialAuth> {
  final _formKey = GlobalKey<FormState>();
  final _signup = SignUp();
  String? _error;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: [
          Text("Identifiant", style: Theme.of(context).textTheme.bodyText1),
          _emailField(),
          Text("Mot de passe", style: Theme.of(context).textTheme.bodyText1),
          _passwordField(),
          _error != null
              ? Text(_error!,
                  style: const TextStyle(color: Colors.red),
                  textAlign: TextAlign.center)
              : const Text(""),
          _connectButton(),
          Text("Mot de passe oublié ?",
              style: Theme.of(context).textTheme.bodyText1),
          _createAccountButton()
        ]));
  }

  _emailField() {
    return _inputFormField(onSaved: (email) {
      _signup.email = email!;
    }, validator: (value) {
      if (value == null || value.isEmpty) {
        return "Veuillez entrer un identifiant valide";
      }
      return null;
    });
  }

  _passwordField() {
    return _inputFormField(
        obscured: true,
        onSaved: (password) {
          _signup.password = password!;
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Veuillez entrer un mot de passe valide";
          }
          return null;
        });
  }

  _connectButton() {
    return _button("Se connecter", onPressed: () async {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();
        try {
          await Authenticator().emailAuth(_signup.email, _signup.password);
          Navigator.pushNamed(context, profileUrl);
        } on FirebaseAuthException catch (e) {
          setState(() {
            _error = e.message;
          });
        }
      }
    });
  }

  _createAccountButton() {
    return _button("Créer un compte", onPressed: () async {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();
        try {
          await Authenticator().emailSignUp(_signup.email, _signup.password);
          Navigator.pushNamed(context, profileUrl);
        } on FirebaseAuthException catch (e) {
          setState(() {
            _error = e.message;
          });
        }
      }
    });
  }

  _inputFormField({onSaved, validator, obscured = false}) {
    return Container(
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.fromLTRB(10, 1, 10, 1),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(40),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: const Offset(0, 3))
            ]),
        width: MediaQuery.of(context).size.width * 0.75,
        child: TextFormField(
            obscureText: obscured,
            onSaved: onSaved,
            decoration: const InputDecoration(border: InputBorder.none),
            validator: validator));
  }

  _button(title, {onPressed}) {
    return TextButton(
        onPressed: null,
        child: Container(
            margin: const EdgeInsets.all(5),
            height: 50,
            padding: const EdgeInsets.fromLTRB(10, 1, 10, 1),
            decoration: BoxDecoration(
                color: primaryColor,
                borderRadius: BorderRadius.circular(40),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3))
                ]),
            width: MediaQuery.of(context).size.width * 0.75,
            child: Center(
                child: TextButton(
              onPressed: onPressed,
              child: Text(title, style: Theme.of(context).textTheme.headline2),
            ))));
  }
}
