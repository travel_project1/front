import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:front/common/const.dart';
import 'package:front/pages/signIn/form.dart';
import 'package:front/services/auth.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPage();
}

class _SignInPage extends State<SignInPage> {
  String? _error;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      Positioned(
          top: -MediaQuery.of(context).size.width * 0.8,
          child: Image(
              width: MediaQuery.of(context).size.width,
              image: const AssetImage("assets/images/logo.png"))),
      Container(
          margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.width * 0.5 + 10),
          child: SingleChildScrollView(
              child: Column(children: [_title(), _getCredentialsFields()])))
    ]));
  }

  _title() {
    return Align(
        alignment: Alignment.center,
        child: Text("Trippy", style: Theme.of(context).textTheme.headline1));
  }

  _getCredentialsFields() {
    return Column(children: [
      const CredentialAuth(),
      _divider(),
      _googleSignIn(),
      _errorField()
    ]);
  }

  _googleSignIn() {
    return TextButton(
        onPressed: null,
        child: Container(
            margin: const EdgeInsets.all(5),
            height: 50,
            padding: const EdgeInsets.fromLTRB(10, 1, 10, 1),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3))
                ]),
            width: MediaQuery.of(context).size.width * 0.75,
            child: Center(
                child: Row(children: [
              Image.asset("assets/images/google_icon.png", height: 35),
              Expanded(
                  child: Center(
                      child: TextButton(
                          onPressed: () async {
                            try {
                              await Authenticator().googleAuth();
                              Navigator.pushNamed(context, profileUrl);
                            } on FirebaseAuthException catch (e) {
                              setState(() {
                                _error = e.message;
                              });
                            }
                          },
                          child: Text("Connexion avec Google",
                              style: Theme.of(context).textTheme.headline6))))
            ]))));
  }

  _errorField() {
    return _error == null
        ? const Text("")
        : Text(_error!, style: const TextStyle(color: Colors.red));
  }

  _divider() {
    return SizedBox(
        width: MediaQuery.of(context).size.width * 0.25,
        child: const Divider(color: Colors.black));
  }
}
