import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/models/group.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/pages/groups/groups_viewer.dart';
import 'package:front/services/groups.dart';

class GroupsPage extends StatelessWidget {
  const GroupsPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _groups = GroupsService().getGroups();

    return Scaffold(
        bottomNavigationBar: const TrippyNavigationBar(groupsIndex),
        body: FutureBuilder(
            future: _groups,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _error();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                return GroupsViewer(groups: snapshot.data as List<Group>);
              }

              return _loader();
            }));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving groups !",
            style: TextStyle(color: Colors.red)));
  }
}
