import 'dart:math';

import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/models/group.dart';
import 'package:front/pages/components/big_icon.dart';
import 'package:front/pages/components/big_icons_grid_view.dart';
import 'package:front/pages/components/search_bar.dart';
import 'package:google_fonts/google_fonts.dart';

class GroupsViewer extends StatefulWidget {
  final List<Group> groups;

  const GroupsViewer({required this.groups, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GroupsViewer();
}

class _GroupsViewer extends State<GroupsViewer> {
  late List<Group> mutableGroups;

  @override
  void initState() {
    super.initState();
    mutableGroups = widget.groups;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SearchBar("un groupe", onChanged: (value) {
        setState(() {
          mutableGroups = widget.groups
              .where((group) =>
                  group.name.toLowerCase().contains(value.toLowerCase()))
              .toList();
        });
      }),
      _title(),
      _groups()
    ]);
  }

  _groups() {
    List<BigIconData> data = [];

    data.add(BigIconData("Créer un groupe", Icons.add, greyColor));
    for (var group in mutableGroups) {
      data.add(BigIconData(group.name, Icons.group_outlined, purpleColor));
    }
    return Flexible(child: BigIconsGridView(items: data));
  }

  _title() {
    return Container(
        margin: const EdgeInsets.fromLTRB(30, 20, 30, 10),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Text("Groupes",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontSize: 28))));
  }
}
