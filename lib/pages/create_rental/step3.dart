import 'package:flutter/material.dart';
import 'package:front/models/rental.dart';
import 'package:front/pages/create_rental/create_rental_step.dart';

class Step3 extends CreateRentalStep {
  TextEditingController startCtrl = TextEditingController();
  TextEditingController endCtrl = TextEditingController();

  Step3(Rental createdRental, GlobalKey<FormState> formState) : super("Disponibilités", createdRental, formState);

  @override
  Widget buildContent(BuildContext context) {
    return Form(
      key: super.formState,
        child: Column(children: [
          _lbl(context, "Date début"),
          Row(children: [Expanded(flex: 2, child: _textField(startCtrl)), Expanded(child: _btn(context, startCtrl))]),
          _separator(),
          _lbl(context, "Date fin"),
          Row(children: [Expanded(flex: 2, child: _textField(endCtrl)), Expanded(child: _btn(context, endCtrl))]),
        ]));
  }
  
  _btn(BuildContext context, TextEditingController ctrl) {
    return IconButton(
        onPressed: () {
      _selectDate(context, ctrl);
    },
    icon: const Icon(Icons.calendar_today_rounded));
  }

  _selectDate(BuildContext context, TextEditingController ctrl) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(const Duration(days: 365)),
    );
    if (selected == null) return;
    ctrl.text = selected.toString();
  }

  _textField(TextEditingController ctrl) {
    return TextFormField(
      controller: ctrl,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisableFocusNode(),
      onSaved: (value) => createdRental.pushAvailabilities(DateTime.parse(value!)),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Invalide";
        }
        return null;
      },
      maxLines: 1);
  }

  _separator() {
    return const SizedBox(height: 10);
  }

  _lbl(BuildContext context, String lbl) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(lbl,
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headline6));
  }

}

class AlwaysDisableFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}