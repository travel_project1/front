import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/models/criteria.dart';
import 'package:front/models/rental.dart';
import 'package:front/models/rental_criteria.dart';
import 'package:front/pages/create_rental/checkbox_field.dart';
import 'package:front/pages/create_rental/create_rental_step.dart';
import 'package:front/pages/create_rental/num_dropdown.dart';
import 'package:front/services/criterias.dart';

class Step2 extends CreateRentalStep {
  Step2(Rental createdRental, GlobalKey<FormState> formKey) : super("Critères obligatoires", createdRental, formKey);

  @override
  Widget buildContent(BuildContext context) {
    return FutureBuilder(
        future: CriteriasService().getCriterias("mandatory"),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return _error();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            return _content(context, snapshot.data as List<Criteria>);
          }

          return _loader();
        });
  }

  _content(BuildContext context, List<Criteria> criterias) {
    List<Widget> childs = [];

    for (Criteria crit in criterias) {
      RentalCriteria rc = RentalCriteria.fromCriteria(crit);
      createdRental.criterias.add(rc);

      List<Widget> line = [];
      line.add(Expanded(child: _lbl(context, crit.name)));
      if (!crit.hasValue) {
        line.add(CheckboxFormField(onSaved: (value) => createdRental.setCriteriaBoolValue(rc.id, value!)));
      } else {
        line.add(StepperNumDropdown(onChanged: (value) => createdRental.setCriteriaIntValue(rc.id, value!), value: 1));
      }
      childs.add(Row(children: line));
      childs.add(_separator());
    }

    return Form(key: super.formState, child: Column(children: childs));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving mandatory criterias !",
            style: TextStyle(color: Colors.red)));
  }

  _separator() {
    return const SizedBox(height: 10);
  }

  _lbl(BuildContext context, String lbl) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(lbl,
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headline6));
  }
}
