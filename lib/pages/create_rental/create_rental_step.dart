import 'package:flutter/material.dart';
import 'package:front/models/rental.dart';

abstract class CreateRentalStep {
  final String title;
  final GlobalKey<FormState> formState;
  final Rental createdRental;

  CreateRentalStep(this.title, this.createdRental, this.formState);

  Widget buildContent(BuildContext context);
}
