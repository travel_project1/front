import 'package:flutter/material.dart';

class StepperFormField extends StatefulWidget {
  final Function(String?) onSaved;
  final int maxLines;

  const StepperFormField({Key? key, required this.onSaved, this.maxLines = 1})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _StepperFormField();
}

class _StepperFormField extends State<StepperFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        onSaved: widget.onSaved,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Invalide";
          }
          return null;
        },
        maxLines: widget.maxLines);
  }
}
