import 'package:flutter/material.dart';
import 'package:front/models/rental.dart';
import 'package:front/pages/create_rental/create_rental_step.dart';
import 'package:front/pages/create_rental/form_field.dart';
import 'package:front/pages/create_rental/num_field.dart';

const double imgHeight = 90;

class Step1 extends CreateRentalStep {
  Step1(Rental createdRental, GlobalKey<FormState> formKey) : super("Informations Générales", createdRental, formKey);

  @override
  Widget buildContent(BuildContext context) {
    return Form(
        key: super.formState,
        child: Column(children: [
          _img(),
          _separator(),
          _lbl(context, "Nom de la location"),
          StepperFormField(onSaved: (name) => createdRental.name = name!),
          _separator(),
          _lbl(context, "Description"),
          StepperFormField(
              maxLines: 5,
              onSaved: (desc) => createdRental.description = desc!),
          _separator(),
          _cPCity(context),
          _separator(),
          _lbl(context, "Adresse"),
          StepperFormField(
              onSaved: (address) => createdRental.address = address!),
          _separator(),
          _lbl(context, "Superficie"),
          StepperNumField(onSaved: (sup) => createdRental.size = int.parse(sup!))
        ]));
  }

  _separator() {
    return const SizedBox(height: 10);
  }

  _cPCity(BuildContext context) {
    return Row(children: [
      Expanded(
          child: Column(children: [
        _lbl(context, "Code postal"),
        StepperNumField(onSaved: (cp) => createdRental.zipCode = cp!)
      ])),
      const SizedBox(width: 20),
      Expanded(
          flex: 2,
          child: Column(children: [
            _lbl(context, "Ville"),
            StepperFormField(onSaved: (city) => createdRental.city = city!)
          ]))
    ]);
  }

  _lbl(BuildContext context, String lbl) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(lbl,
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headline6));
  }

  _img() {
    return ClipOval(
        child: Image.asset("assets/images/preview_1.jpg", height: imgHeight));
  }
}
