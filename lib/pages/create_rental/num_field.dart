import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class StepperNumField extends StatelessWidget {
  final Function(String?) onSaved;

  const StepperNumField({Key? key, required this.onSaved}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        keyboardType: TextInputType.number,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        onSaved: onSaved,
        initialValue: "1",
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Invalide";
          }
          return null;
        });
  }
}
