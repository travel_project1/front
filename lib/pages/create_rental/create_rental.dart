import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/models/rental.dart';
import 'package:front/pages/components/rounded_rect_background.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/pages/create_rental/create_rental_step.dart';
import 'package:front/pages/create_rental/step1.dart';
import 'package:front/pages/create_rental/step2.dart';
import 'package:front/pages/create_rental/step3.dart';
import 'package:front/services/rentals.dart';

const int maxSteps = 2;

class CreateRentalPage extends StatefulWidget {
  const CreateRentalPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CreateRentalPage();
}

class _CreateRentalPage extends State<CreateRentalPage> {
  int _currentStep = 0;
  bool _loading = false;

  final Rental _createdRental = Rental.empty();
  final List<CreateRentalStep> steps = [];

  @override
  void initState() {
    super.initState();
    steps.add(Step1(_createdRental, GlobalKey<FormState>()));
    steps.add(Step2(_createdRental, GlobalKey<FormState>()));
    steps.add(Step3(_createdRental, GlobalKey<FormState>()));
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Scaffold(
        bottomNavigationBar: const TrippyNavigationBar(profileIndex),
        body: CustomPaint(
            painter: RoundedRectBackground(),
            child: SizedBox(
                width: _width,
                child: Container(
                    margin: const EdgeInsets.only(top: 50),
                    child: Column(children: [_title(), _stepper()])))));
  }

  _stepper() {
    return Flexible(
        child: Container(
            margin: const EdgeInsets.only(top: 20),
            child: Stepper(
                type: StepperType.vertical,
                physics: const ScrollPhysics(),
                currentStep: _currentStep,
                onStepTapped: (step) => _tapped(step),
                onStepContinue: _continued,
                onStepCancel: _canceled,
                controlsBuilder:
                    (BuildContext context, ControlsDetails details) {
                  return Container(
                      margin: const EdgeInsets.only(top: 16.0),
                      child: ConstrainedBox(
                          constraints:
                              const BoxConstraints.tightFor(height: 48.0),
                          child: Row(children: [
                            TextButton(
                                onPressed: details.onStepContinue,
                                child: Text("CONTINUER",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .apply(color: blueColor))),
                            TextButton(
                                onPressed: details.onStepCancel,
                                child: Text("PRECEDENT",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .apply(color: Colors.red))),
                            _loading
                                ? const CircularProgressIndicator(
                                    color: purpleColor)
                                : Container()
                          ])));
                },
                steps: List.generate(
                    steps.length,
                    (index) => Step(
                        title: Text(steps[index].title),
                        content: steps[index].buildContent(context))))));
  }

  _title() {
    return Text("Créer une location",
        style: Theme.of(context).textTheme.headline2);
  }

  _tapped(int step) {
    GlobalKey<FormState> currentFormKey = steps[_currentStep].formState;

    if (currentFormKey.currentState!.validate()) {
      currentFormKey.currentState!.save();

      setState(() => _currentStep = step);
    }
  }

  _continued() async {
    GlobalKey<FormState> currentFormKey = steps[_currentStep].formState;

    if (currentFormKey.currentState!.validate()) {
      currentFormKey.currentState!.save();
      if (_currentStep != 2) {
        setState(() {
          _currentStep += 1;
        });
      } else {
        setState(() {
          _loading = true;
        });
        await RentalsService().createRental(_createdRental);
        setState(() {
          _loading = false;
        });
        Navigator.pushNamed(context, myRentalsUrl);
      }
    }
  }

  _canceled() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }
}
