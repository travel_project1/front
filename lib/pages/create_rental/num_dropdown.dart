import 'package:flutter/material.dart';

const int maxRooms = 10;

class StepperNumDropdown extends StatefulWidget {
  final Function(int?) onChanged;
  final int value;

  const StepperNumDropdown(
      {Key? key, required this.onChanged, required this.value})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _StepperNumDropdown();
}

class _StepperNumDropdown extends State<StepperNumDropdown> {
  late int _currentValue;

  @override
  void initState() {
    super.initState();
    _currentValue = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
        value: _currentValue,
        onChanged: _onChanged,
        items: List.generate(
            maxRooms + 1,
            (index) =>
                DropdownMenuItem(child: Text(index.toString()), value: index)));
  }

  _onChanged(int? value) {
    setState(() {
      _currentValue = value!;
    });
    widget.onChanged(value);
  }
}
