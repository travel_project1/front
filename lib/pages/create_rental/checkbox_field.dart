import 'package:flutter/material.dart';

class CheckboxFormField extends StatefulWidget {
  final Function(bool?) onSaved;

  const CheckboxFormField({Key? key, required this.onSaved}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CheckboxFormField();
}

class _CheckboxFormField extends State<CheckboxFormField> {
  late bool _currentValue;

  @override
  void initState() {
    super.initState();
    _currentValue = true;
  }

  @override
  Widget build(BuildContext context) {
    return Checkbox(value: _currentValue, onChanged: _onChanged);
  }

  _onChanged(bool? value) {
    setState(() {
      _currentValue = value!;
    });
    widget.onSaved(value);
  }
}
