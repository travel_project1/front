import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/pages/components/card_footer.dart';
import 'package:google_fonts/google_fonts.dart';

class TextContainer extends StatelessWidget {
  final String title;
  final String? description;
  const TextContainer(this.title,
      {this.description, Key? key, CardFooter? cardFooter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 15),
      child: Column(
        children: [
          Expanded(
              flex: 1,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(title,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.poppins(
                          color: greyColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w300)))),
          description != null
              ? Expanded(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(description ?? '',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: GoogleFonts.poppins(
                              color: greyColor,
                              fontSize: 10,
                              fontWeight: FontWeight.w300))))
              : Container()
        ],
      ),
    );
  }
}
