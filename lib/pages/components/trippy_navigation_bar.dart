import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/services/auth.dart';

class NavigationItem {
  final Icon icon;
  final String name;
  final String url;
  final Color color;

  NavigationItem(this.icon, this.name, this.url, this.color);
}

final List<NavigationItem> items = [
  NavigationItem(const Icon(Icons.search), "Recherche", homeUrl, blueColor),
  NavigationItem(
      const Icon(Icons.group_outlined), "Groupes", groupsUrl, purpleColor),
  NavigationItem(
      const Icon(Icons.favorite_outline), "Favoris", favoritesUrl, pinkColor),
  NavigationItem(const Icon(Icons.description_outlined), "Réservations",
      reservationsUrl, blueColor),
  NavigationItem(const Icon(Icons.person), "profil", profileUrl, blueColor)
];

class TrippyNavigationBar extends StatefulWidget {
  final int navIndex;
  const TrippyNavigationBar(this.navIndex, {Key? key}) : super(key: key);

  @override
  State<TrippyNavigationBar> createState() => _TrippyNavigationBar();
}

class _TrippyNavigationBar extends State<TrippyNavigationBar> {
  final authenticator = Authenticator();
  final List<BottomNavigationBarItem> _navItems = [];

  @override
  void initState() {
    super.initState();
    addItems();
  }

  void _onItemTapped(NavigationItem item) {
    Navigator.pushReplacementNamed(context, item.url);
  }

  void addItems() {
    for (var item in items) {
      _navItems.add(BottomNavigationBarItem(icon: item.icon, label: ""));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: _navItems,
      backgroundColor: Color(0xffffffff),
      type: BottomNavigationBarType.fixed,
      showUnselectedLabels: false,
      showSelectedLabels: false,
      currentIndex: widget.navIndex,
      selectedItemColor: items[widget.navIndex].color,
      unselectedItemColor: Colors.grey,
      onTap: (index) => _onItemTapped(items[index]),
    );
  }
}
