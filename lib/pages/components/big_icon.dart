import 'package:flutter/material.dart';

const double iconSize = 90;

class BigIconData {
  final String name;
  final IconData icon;
  final Color color;
  final Function(BuildContext)? callback;

  BigIconData(this.name, this.icon, this.color, {this.callback});
}

class BigIcon extends StatelessWidget {
  final BigIconData data;

  const BigIcon({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [_icon(context), _name(context)]);
  }

  _icon(context) {
    return GestureDetector(
        onTap: _callback(context),
        child: Container(
            margin: const EdgeInsets.only(bottom: 10),
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: data.color,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 8,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: Icon(data.icon, size: iconSize, color: Colors.white)));
  }

  _name(context) {
    return Text(data.name,
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: Theme.of(context).textTheme.bodyText1);
  }

  _callback(context) {
    if (data.callback == null) {
      return () {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("Not implemented",
                style: TextStyle(fontWeight: FontWeight.bold))));
      };
    }

    return () {
      data.callback!(context);
    };
  }
}
