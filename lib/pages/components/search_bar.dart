import 'package:flutter/material.dart';
import 'package:front/common/const.dart';

class SearchBar extends StatelessWidget {
  final String label;
  final Function(String)? onChanged;

  const SearchBar(this.label, {this.onChanged, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: topPageMargin),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(40),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  blurRadius: 8,
                  offset: const Offset(0, 2))
            ]),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Container(
            margin: const EdgeInsets.only(top: 5),
            child: TextField(
                onChanged: onChanged,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: const Icon(Icons.search),
                    hintText: 'Rechercher ' + label))));
  }
}
