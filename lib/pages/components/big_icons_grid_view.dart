import 'package:flutter/material.dart';
import 'package:front/pages/components/big_icon.dart';

class BigIconsGridView extends StatelessWidget {
  final List<BigIconData> items;

  const BigIconsGridView({required this.items, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        primary: false,
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        padding: const EdgeInsets.all(20),
        crossAxisCount: 2,
        children: List.generate(
            items.length, (index) => BigIcon(data: items[index])));
  }
}
