import 'package:flutter/material.dart';

class CircleBackground extends CustomPainter {
  final Color color;

  const CircleBackground(this.color, {Key? key});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    canvas.drawCircle(
        Offset(size.width / 2, -size.width * 0.3), size.width * 0.6, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
