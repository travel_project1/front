import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/models/room.dart';
import 'package:front/models/rental_criteria.dart';
import 'package:google_fonts/google_fonts.dart';

class CardFooter extends StatelessWidget {
  final int size;
  final List<RentalCriteria> criterias;

  const CardFooter({
    Key? key,
    required this.size,
    required this.criterias,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
          padding: EdgeInsets.only(left: 15, bottom: 10),
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Superficie : $size m²",
                      style: GoogleFonts.poppins(
                          color: greyColor,
                          fontSize: 10,
                          fontWeight: FontWeight.w300),
                    )),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Equipements: " + criterias.join(" - "),
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.poppins(
                          color: greyColor,
                          fontSize: 10,
                          fontWeight: FontWeight.w300),
                    ))
              ],
            )));
  }
}
