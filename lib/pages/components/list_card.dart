import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ListCard extends StatefulWidget {
  final String imagePath;
  final Widget textContainer;
  final Widget? cardFooter;
  const ListCard(this.imagePath, this.textContainer,
      {Key? key, this.cardFooter})
      : super(key: key);

  @override
  _ListCardState createState() => _ListCardState();
}

class _ListCardState extends State<ListCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 15, right: 15),
      height: widget.cardFooter != null
          ? MediaQuery.of(context).size.height * 0.20
          : MediaQuery.of(context).size.height * 0.132,
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              blurRadius: 8,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: Card(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        alignment: Alignment.topLeft,
                        padding: const EdgeInsets.only(left: 10, top: 10),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Image.asset(
                            widget.imagePath,
                            fit: BoxFit.fitHeight,
                            height: 80,
                            width: 80,
                          ),
                        )),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.7 -
                          20, // Dégueulasse, à améliorer
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(left: 10),
                      child: SizedBox(
                          height: MediaQuery.of(context).size.height * 0.108,
                          child: widget.textContainer),
                    )
                  ],
                ),
                widget.cardFooter ?? Container()
              ],
            )),
      ),
    );
  }
}
