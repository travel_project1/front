import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';

class RoundedRectBackground extends CustomPainter {
  RoundedRectBackground({Key? key});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = blueColor
      ..style = PaintingStyle.fill;

    canvas.drawRRect(
        RRect.fromRectAndRadius(Rect.fromLTWH(0, -100, size.width, 200),
            const Radius.circular(15.0)),
        paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
