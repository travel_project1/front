import 'package:flutter/material.dart';
import 'package:front/common/const.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/services/auth.dart';

class PersonalInfoPage extends StatelessWidget {
  const PersonalInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: const TrippyNavigationBar(profileIndex),
        body: Container(
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.only(top: 40),
            child: Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Container(
                    margin: const EdgeInsets.only(right: 10),
                    child: _profileImage())
              ]),
              _displayName(),
              _emailAddress(),
              _phoneNumber()
            ])));
  }
}

_displayName() {
  return Authenticator().authenticatedUser!.displayName != null
      ? PersonalInfoField(
          "Nom", Authenticator().authenticatedUser!.displayName!)
      : Container();
}

_profileImage() {
  return ClipOval(
      child: Authenticator().authenticatedUser!.photoURL != null
          ? Image.network(Authenticator().authenticatedUser!.photoURL!,
              height: 90)
          : Image.network(
              "https://eu.ui-avatars.com/api/?name=${Authenticator().authenticatedUser!.email!}",
              height: 90));
}

_emailAddress() {
  return PersonalInfoField(
      "Adresse e-mail", Authenticator().authenticatedUser!.email!);
}

_phoneNumber() {
  return Authenticator().authenticatedUser!.phoneNumber != null
      ? PersonalInfoField("Numéro de téléphone",
          Authenticator().authenticatedUser!.phoneNumber!)
      : Container();
}

class PersonalInfoField extends StatelessWidget {
  final String label;
  final String value;

  const PersonalInfoField(this.label, this.value, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 30),
        child: Column(children: [
          Container(
            child: Align(
                alignment: Alignment.centerLeft,
                child:
                    Text(label, style: Theme.of(context).textTheme.headline6)),
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: TextField(
                enabled: false,
                decoration: InputDecoration(
                  labelText: value,
                ),
              ))
        ]));
  }
}
