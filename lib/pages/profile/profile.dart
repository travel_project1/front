import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/pages/components/big_icon.dart';
import 'package:front/pages/components/big_icons_grid_view.dart';
import 'package:front/pages/components/circle_background.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/services/auth.dart';

const double imgHeight = 90;

final List<BigIconData> options = [
  BigIconData("Mes Informations", Icons.person_outline, blueColor,
      callback: (context) {
    Navigator.pushNamed(context, personalInfoUrl);
  }),
  BigIconData("Critères", Icons.checklist, blueColor),
  BigIconData("Facturation", Icons.receipt_outlined, blueColor),
  BigIconData("Mes locations", Icons.home, blueColor, callback: (context) {
    Navigator.pushNamed(context, myRentalsUrl);
  })
];

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
        body: CustomPaint(
            painter: const CircleBackground(blueColor),
            child: SizedBox(
                width: width,
                child: Column(children: [
                  _profileImage(width),
                  _profileName(context),
                  _options(width, context)
                ]))),
        bottomNavigationBar: const TrippyNavigationBar(profileIndex));
  }

  _options(width, context) {
    return Flexible(child: BigIconsGridView(items: options));
  }

  _profileName(context) {
    final user = Authenticator().authenticatedUser!;

    return Container(
        margin: const EdgeInsets.all(30),
        child: Text(user.displayName ?? user.email!,
            style: const TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.black,
                fontFamily: "Montserrat-Regular",
                fontSize: 28)));
  }

  _profileImage(width) {
    return Container(
        margin: EdgeInsets.only(top: width * 0.3 - imgHeight / 2),
        child: ClipOval(
            child: Authenticator().authenticatedUser!.photoURL != null
                ? Image.network(Authenticator().authenticatedUser!.photoURL!,
                    height: 90)
                : Image.network(
                    "https://eu.ui-avatars.com/api/?name=${Authenticator().authenticatedUser!.email!}",
                    height: 90)));
  }
}
