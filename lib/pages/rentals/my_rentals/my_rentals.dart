import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/models/rental.dart';
import 'package:front/pages/components/card_footer.dart';
import 'package:front/pages/components/list_card.dart';
import 'package:front/pages/components/text_container.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/services/rentals.dart';

class MyRentalsPage extends StatelessWidget {
  const MyRentalsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _rentals = RentalsService().getRentals();

    return Scaffold(
        bottomNavigationBar: const TrippyNavigationBar(profileIndex),
        body: FutureBuilder(
            future: _rentals,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _error();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                return _content(context, snapshot.data as List<Rental>);
              }

              return _loader();
            }));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving rentals !",
            style: TextStyle(color: Colors.red)));
  }

  _content(BuildContext context, List<Rental> rentals) {
    return Column(children: [
      Container(
        margin: const EdgeInsets.only(left: 20, right: 20, top: 30),
        padding: const EdgeInsets.only(top: 20, bottom: 15),
        child: Row(children: [
          Expanded(
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Résultats (${rentals.length})",
                      style: Theme.of(context).textTheme.headline5))),
          IconButton(
              icon: const Icon(Icons.add),
              onPressed: () => Navigator.pushNamed(context, createRentalUrl))
        ]),
      ),
      Flexible(
          child: MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: ListView.builder(
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            itemCount: rentals.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () => {
                  Navigator.of(context).pushNamed(
                      "$myRentalsUrl/${rentals[index].id.toString()}")
                },
                child: ListCard(
                    rentals[index].imagePath,
                    TextContainer(rentals[index].name,
                        description: rentals[index].description),
                    cardFooter: CardFooter(
                      size: rentals[index].size,
                      criterias: rentals[index].criterias,
                    )),
              );
            }),
      ))
    ]);
  }
}
