import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/models/equipment.dart';
import 'package:front/models/rental.dart';
import 'package:front/models/room.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/services/rentals.dart';

class MyRentalPage extends StatelessWidget {
  final String rentalId;
  const MyRentalPage({Key? key, required this.rentalId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _rental = RentalsService().getRental(rentalId);

    return Scaffold(
        bottomNavigationBar: const TrippyNavigationBar(profileIndex),
        body: FutureBuilder(
            future: _rental,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _error();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                return _content(context, snapshot.data as Rental);
              }

              return _loader();
            }));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving rental !",
            style: TextStyle(color: Colors.red)));
  }

  _content(BuildContext context, Rental rental) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _backgroundImage(context, rental),
          Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  _title(context, rental),
                  _owner(context),
                  _description(rental),
                  _equipementsTitle(context),
                  _equipementsContent(rental)
                ],
              ))
        ],
      ),
    );
  }

  _backgroundImage(BuildContext context, Rental rental) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Image.asset(rental.imagePath, fit: BoxFit.fitWidth));
  }

  _title(BuildContext context, Rental rental) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(rental.name, style: Theme.of(context).textTheme.headline2));
  }

  _owner(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 20),
        child: Row(
          children: [
            CircleAvatar(
                foregroundImage:
                    Image.asset("assets/images/landscape.jpeg").image),
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Proposé par Guillaume LAFOSSE",
                      style: Theme.of(context).textTheme.bodyText1)),
            )
          ],
        ));
  }

  _description(Rental rental) {
    return Container(
      margin: const EdgeInsets.only(top: 25),
      child: Align(
          alignment: Alignment.centerLeft, child: Text(rental.description)),
    );
  }

  _equipementsTitle(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 30, bottom: 10),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Text("Equipements",
                style: Theme.of(context).textTheme.headline6)));
  }

  _equipementsContent(Rental rental) {
    return Column(
        children: List.generate(
            rental.criterias.length,
            (index) => Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    padding: const EdgeInsets.only(top: 3, bottom: 3),
                    child: Text(rental.criterias[index].name)))));
  }
}
