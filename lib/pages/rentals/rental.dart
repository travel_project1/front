import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/models/criteria.dart';
import 'package:front/models/booking.dart';
import 'package:front/models/equipment.dart';
import 'package:front/models/rental.dart';
import 'package:front/models/room.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/services/bookings.dart';
import 'package:front/services/rentals.dart';

class RentalPage extends StatefulWidget {
  final String rentalId;

  const RentalPage({Key? key, required this.rentalId}) : super(key: key);

  @override
  State<RentalPage> createState() => _RentalPageState();
}

class _RentalPageState extends State<RentalPage> {
  final Booking _createdBooking = Booking.empty();
  DateTime beginDate = DateTime.now();
  DateTime endDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    final _rental = RentalsService().getRental(widget.rentalId);

    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: _getFloatingButtonAction(),
        bottomNavigationBar: const TrippyNavigationBar(homeIndex),
        body: FutureBuilder(
            future: _rental,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _error();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                return _content(context, snapshot.data as Rental);
              }

              return _loader();
            }));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving rental !",
            style: TextStyle(color: Colors.red)));
  }

  _content(BuildContext context, Rental rental) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _backgroundImage(context, rental),
          Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  _title(context, rental),
                  _owner(context),
                  _description(rental),
                  _equipementsTitle(context),
                  _equipementsContent(rental)
                ],
              ))
        ],
      ),
    );
  }

  _backgroundImage(BuildContext context, Rental rental) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Image.asset(rental.imagePath, fit: BoxFit.fitWidth));
  }

  _title(BuildContext context, Rental rental) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(rental.name, style: Theme.of(context).textTheme.headline2));
  }

  _owner(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 20),
        child: Row(
          children: [
            CircleAvatar(
                foregroundImage:
                    Image.asset("assets/images/landscape.jpeg").image),
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Proposé par Guillaume LAFOSSE",
                      style: Theme.of(context).textTheme.bodyText1)),
            )
          ],
        ));
  }

  _description(Rental rental) {
    return Container(
      margin: const EdgeInsets.only(top: 25),
      child: Align(
          alignment: Alignment.centerLeft, child: Text(rental.description)),
    );
  }

  _equipementsTitle(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 30, bottom: 10),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Text("Equipements",
                style: Theme.of(context).textTheme.headline6)));
  }

  _equipementsContent(Rental rental) {
    return Column(
        children: List.generate(
            rental.criterias.length,
            (index) => Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    padding: const EdgeInsets.only(top: 3, bottom: 3),
                    child: Text(rental.criterias[index].name)))));
  }

  Future<void> _selectDate(BuildContext context, String type) async {
    if (type == 'begin') {
      final DateTime? picked = await showDatePicker(
          context: context,
          helpText: "Sélectionnez une date de début",
          initialDate: DateTime.now(),
          confirmText: "CONTINUER",
          initialDatePickerMode: DatePickerMode.day,
          firstDate: DateTime(DateTime.now().year),
          lastDate: DateTime(DateTime.now().year + 10));
      if (picked != null) {
        setState(() {
          beginDate = picked;
        });
        _selectDate(context, "end");
      }
    } else {
      final DateTime? picked = await showDatePicker(
          context: context,
          helpText: "Sélectionnez une date de fin",
          initialDate: DateTime.now(),
          confirmText: "VALIDER",
          initialDatePickerMode: DatePickerMode.day,
          firstDate: DateTime(DateTime.now().year),
          lastDate: DateTime(DateTime.now().year + 10));
      if (picked != null) {
        setState(() {
          endDate = picked;
          _createdBooking.begin = beginDate;
          _createdBooking.end = endDate;
          _createdBooking.rentalId = int.parse(widget.rentalId);
        });
        await BookingsService().createBooking(_createdBooking);
        Navigator.of(context).pushNamed(
            '/rentals/' + widget.rentalId + '/booking',
            arguments: {"beginDate": beginDate, "endDate": endDate});
      }
    }
  }

  _getFloatingButtonAction() {
    return FloatingActionButton.extended(
      onPressed: () {
        _selectDate(context, "begin");
      },
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(50.0))),
      label: Container(
        width: MediaQuery.of(context).size.width * 0.4,
        child: const Text("Réserver"),
        alignment: Alignment.center,
      ),
      backgroundColor: const Color(0xff67f5e0),
    );
  }
}
