import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/models/rental.dart';
import 'package:front/services/rentals.dart';
import 'package:intl/intl.dart';

class BookingConfirmationPage extends StatefulWidget {
  final String rentalId;

  const BookingConfirmationPage({
    Key? key,
    required this.rentalId,
  }) : super(key: key);

  @override
  State<BookingConfirmationPage> createState() =>
      _BookingConfirmationPageState();
}

class _BookingConfirmationPageState extends State<BookingConfirmationPage> {
  @override
  Widget build(BuildContext context) {
    final _rental = RentalsService().getRental(widget.rentalId);

    return Scaffold(
        body: FutureBuilder(
            future: _rental,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _error();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                return _content(context, snapshot.data as Rental);
              }

              return _loader();
            }));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving rental !",
            style: TextStyle(color: Colors.red)));
  }

  _content(BuildContext context, Rental rental) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _backgroundImage(context, rental),
          Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  _title(context, rental),
                  const SizedBox(
                    height: 30,
                  ),
                  _recapLabel(context),
                  _bookingDates(),
                  _passengerNumber(),
                  SizedBox(height: 20.0),
                  _confirmButton()
                ],
              ))
        ],
      ),
    );
  }

  _title(BuildContext context, Rental rental) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(rental.name, style: Theme.of(context).textTheme.headline2));
  }

  _backgroundImage(BuildContext context, Rental rental) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Image.asset(rental.imagePath, fit: BoxFit.fitWidth));
  }

  _recapLabel(BuildContext context) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text("Votre voyage :",
            style: Theme.of(context).textTheme.headline2));
  }

  _bookingDates() {
    final args = ModalRoute.of(context)!.settings.arguments as Map;
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(top: 25),
      height: MediaQuery.of(context).size.height * 0.20,
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              blurRadius: 8,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: Card(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Container(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: const Icon(
                                Icons.calendar_month,
                                size: 40.0,
                              ))
                        ],
                      ),
                      Expanded(
                        child: Container(
                            width: MediaQuery.of(context).size.width * 0.7 - 11,
                            alignment: Alignment.center,
                            child: SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.108,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text("Arrivée : 14:00 - Départ 12:00",
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2),
                                    Text(
                                        "${DateFormat('dd / MM').format(args['beginDate'])} - ${DateFormat('dd / MM').format(args['endDate'])}",
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5),
                                  ],
                                ))),
                      ),
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }

  _passengerNumber() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(top: 25),
      height: MediaQuery.of(context).size.height * 0.20,
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              blurRadius: 8,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: Card(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Container(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: const Icon(
                                Icons.person,
                                size: 40.0,
                              ))
                        ],
                      ),
                      Expanded(
                        child: Container(
                            width: MediaQuery.of(context).size.width * 0.7 - 11,
                            alignment: Alignment.center,
                            child: SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.108,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("1 voyageur",
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5),
                                  ],
                                ))),
                      ),
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }

  _confirmButton() {
    return ElevatedButton(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.4,
        child: Text(
          "Confirmer",
          style: Theme.of(context).textTheme.headline4,
        ),
        alignment: Alignment.center,
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/bookings');
      },
      style: ButtonStyle(
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
              const EdgeInsets.all(15.0)),
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          backgroundColor:
              MaterialStateProperty.all<Color>(const Color(0xff67f5e0)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
          ))),
    );
  }
}
