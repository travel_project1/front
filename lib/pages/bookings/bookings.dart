import 'package:flutter/material.dart';
import 'package:front/common/const.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';

class ReservationsPage extends StatelessWidget {
  const ReservationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: Text("Reservations page !")),
      bottomNavigationBar: TrippyNavigationBar(reservationsIndex),
    );
  }
}
