import 'package:flutter/material.dart';
import 'package:front/common/app_colors.dart';
import 'package:front/common/const.dart';
import 'package:front/models/rental.dart';
import 'package:front/pages/components/list_card.dart';
import 'package:front/pages/components/search_bar.dart';
import 'package:front/pages/components/text_container.dart';
import 'package:front/pages/components/trippy_navigation_bar.dart';
import 'package:front/services/favorites.dart';

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _favorites = FavoritesService().getFavorites();

    return Scaffold(
        bottomNavigationBar: const TrippyNavigationBar(favoritesIndex),
        body: FutureBuilder(
            future: _favorites,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return _error();
              }

              if (snapshot.connectionState == ConnectionState.done) {
                return _content(context, snapshot.data as List<Rental>);
              }

              return _loader();
            }));
  }

  _loader() {
    return const Center(child: CircularProgressIndicator(color: blueColor));
  }

  _error() {
    return const Center(
        child: Text("Error retreving favorites !",
            style: TextStyle(color: Colors.red)));
  }

  _content(BuildContext context, List<Rental> favorites) {
    return Column(children: [
      const SearchBar("un favori"),
      Container(
        margin: const EdgeInsets.only(left: 20, right: 20),
        padding: const EdgeInsets.only(top: 20, bottom: 15),
        child: Row(
          children: [
            Text("Résultats (${favorites.length})",
                style: Theme.of(context).textTheme.headline2),
            const Expanded(
                child: Align(
                    child: Icon(Icons.settings_outlined),
                    alignment: Alignment.centerRight))
          ],
        ),
      ),
      Flexible(
          child: MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: ListView.builder(
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            itemCount: favorites.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () => {
                  Navigator.of(context)
                      .pushNamed("$homeUrl/${favorites[index].id.toString()}")
                },
                child: ListCard(
                  favorites[index].imagePath,
                  TextContainer(favorites[index].name),
                ),
              );
            }),
      ))
    ]);
  }
}
