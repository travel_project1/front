import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:front/common/const.dart';
import 'package:front/routers/router.dart';
import 'package:front/common/app_colors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const TrippyApp());
}

class TrippyApp extends StatelessWidget {
  const TrippyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final router = getRouter();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
        initialRoute: signUrl,
        title: 'Travel Project',
        theme: ThemeData(
            textTheme: const TextTheme(
                bodyText1: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: greyColor,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 14),
                headline5: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: greyColor,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 24),
                headline2: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.white,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 34),
                bodyText2: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: greyColor,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 15),
                headline4: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.white,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 15),
                headline6: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 18),
                headline1: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: primaryColor,
                    fontFamily: "Montserrat-Regular",
                    fontSize: 48))),
        onGenerateRoute: router.generator);
  }
}
